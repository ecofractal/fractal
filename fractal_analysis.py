#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 10:31:39 2019

@author: s_tatus

"""

import numpy as np
import scipy
import fractaL.core as fractal


img = scipy.misc.imread("sample_data/img/box2.png")
img = fractal.rgb2gray(img)

#90 is the threshold that will vary accordinly to the picture
img = fractal.fracdim(img, 90)


print("Minkowski–Bouligand dimension (computed): ", img)
print("Haussdorf dimension (theoretical):        ", (np.log(3)/np.log(2)))


